package com.test.demo.mockitojunit.services.implementation;

import com.test.demo.mockitojunit.entities.Item;
import com.test.demo.mockitojunit.repository.ItemRepository;
import com.test.demo.mockitojunit.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService{

    private final ItemRepository itemRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> getAll() {
        return this.itemRepository.findAll();
    }

    @Override
    public List<Item> getByOrder(String orderId) {
        return new ArrayList<>();
    }
}
