package com.test.demo.mockitojunit.services;

import com.test.demo.mockitojunit.entities.Item;

import java.util.List;

public interface ItemService {

    List<Item> getAll();

    List<Item> getByOrder(String orderId);
}
