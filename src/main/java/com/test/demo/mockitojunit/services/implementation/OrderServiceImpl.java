package com.test.demo.mockitojunit.services.implementation;

import com.test.demo.mockitojunit.dto.OrderDTO;
import com.test.demo.mockitojunit.entities.Item;
import com.test.demo.mockitojunit.entities.Order;
import com.test.demo.mockitojunit.repository.ItemRepository;
import com.test.demo.mockitojunit.repository.OrderRepository;
import com.test.demo.mockitojunit.services.ItemService;
import com.test.demo.mockitojunit.services.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ItemService itemService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, ItemService itemService) {

        this.orderRepository = orderRepository;
        this.itemService = itemService;
    }

    @Override
    public List<Order> getAll() {
        return this.orderRepository.findAll();
    }

    @Override
    public Order getById(String id) throws Exception {

        Optional<Order> optionalEntity = this.orderRepository.findById(id);

        if(optionalEntity.isPresent())
            return optionalEntity.get();
        else
            throw new Exception("Order does not exist");
    }

    @Override
    public OrderDTO post(OrderDTO orderDTO) {

        Order orderEntity = new Order();
        BeanUtils.copyProperties(orderEntity, orderDTO);
        this.orderRepository.save(orderEntity);
        orderDTO.setId(orderEntity.getId());

        return orderDTO;
    }

    @Override
    public long calculateTotalAmount(String id) throws Exception {

        //Order order = this.getById(id);

        //List<Item> itemList = this.itemService.getByOrder(order.getId());
        List<Item> itemList = this.itemService.getByOrder("1");

        AtomicLong totalAmount = new AtomicLong(0l);

        itemList.forEach(item -> {

            totalAmount.addAndGet(item.getQuantity() * item.getPrice());
        });

        return totalAmount.longValue();
    }
}
