package com.test.demo.mockitojunit.services;

import com.test.demo.mockitojunit.dto.OrderDTO;
import com.test.demo.mockitojunit.entities.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    Order getById(String id) throws Exception;

    OrderDTO post(OrderDTO orderDTO);

    long calculateTotalAmount(String id) throws Exception;
}
