package com.test.demo.mockitojunit.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ItemDTO {

    private String id;
    private String orderId;
    private String name;
    private String sku;
    private Integer quantity;
    private Integer price;

}
