package com.test.demo.mockitojunit.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper=false)
public class BasicResponseDTO implements Serializable{

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4679862871424143447L;

    public BasicResponseDTO(String message){

        this.message = message;
    }

    String message;

}

