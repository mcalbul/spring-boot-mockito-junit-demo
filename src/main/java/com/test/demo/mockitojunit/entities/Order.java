package com.test.demo.mockitojunit.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "#{@environment.getProperty('mongodb.collection.order')}")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Order {

    @Id
    @JsonProperty("_id")
    private String id;
    @Field("status")
    private String status;
    @JsonProperty("total_amount")
    @Field("total_amount")
    private Integer totalAmount;

}
