package com.test.demo.mockitojunit.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "#{@environment.getProperty('mongodb.collection.item')}")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Item {

    @Id
    @JsonProperty("_id")
    private String id;
    @JsonProperty("order_id")
    @Field("order_id")
    private String orderId;
    @Field("name")
    private String name;
    @Field("sku")
    private String sku;
    @Field("quantity")
    private Integer quantity;
    @Field("price")
    private Integer price;

}
