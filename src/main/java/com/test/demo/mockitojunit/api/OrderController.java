package com.test.demo.mockitojunit.api;

import com.test.demo.mockitojunit.dto.BasicResponseDTO;
import com.test.demo.mockitojunit.dto.OrderDTO;
import com.test.demo.mockitojunit.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping()
    public ResponseEntity<?> get() {

        try {
            return new ResponseEntity<>(this.orderService.getAll(), HttpStatus.OK);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new BasicResponseDTO(e.getMessage()));
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> post(@RequestBody OrderDTO orderDTO){

        try {
            return new ResponseEntity<>(this.orderService.post(orderDTO), HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new BasicResponseDTO(e.getMessage()));
        }
    }
}
