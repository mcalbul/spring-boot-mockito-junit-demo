package com.test.demo.mockitojunit;

import com.test.demo.mockitojunit.api.ItemController;
import com.test.demo.mockitojunit.api.OrderController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MockitoJunitApplicationTests {

	private final OrderController orderController;
	private final ItemController itemController;

	@Autowired
	public MockitoJunitApplicationTests(OrderController orderController, ItemController itemController) {
		this.orderController = orderController;
		this.itemController = itemController;
	}

	@Test
	void contextLoads() {

		assertThat(this.orderController).isNotNull();
		assertThat(this.itemController).isNotNull();
	}

}
