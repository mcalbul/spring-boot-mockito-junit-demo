package com.test.demo.mockitojunit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.demo.mockitojunit.api.OrderController;
import com.test.demo.mockitojunit.dto.OrderDTO;
import com.test.demo.mockitojunit.services.OrderService;
import org.hamcrest.core.AllOf;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(OrderController.class)
public class OrderMockTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService service;

    @Test
    public void testPostOrder() throws Exception {

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setStatus("FAILED");
        orderDTO.setTotalAmount(36500);

        when(service.post(orderDTO)).thenReturn(orderDTO);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/order")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(orderDTO))
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status",
                        AllOf.allOf(
                                StringContains.containsString("FAILED")
                        )));
    }

}
