package com.test.demo.mockitojunit;

import com.test.demo.mockitojunit.entities.Item;
import com.test.demo.mockitojunit.services.ItemService;
import com.test.demo.mockitojunit.services.implementation.OrderServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private ItemService itemService;

    @Test
    public void testCalculateTotal_simpleInput() throws Exception {

        List<Item> itemList = new ArrayList<>();

        Item item = new Item();
        item.setOrderId("1");
        item.setName("uva");
        item.setSku("P123");
        item.setQuantity(2);
        item.setPrice(2200);
        itemList.add(item);

        item = new Item();
        item.setOrderId("1");
        item.setName("peras");
        item.setSku("P124");
        item.setQuantity(5);
        item.setPrice(880);
        itemList.add(item);

        when(this.itemService.getByOrder("1")).thenReturn(itemList);
        Assertions.assertEquals(3000, this.orderService.calculateTotalAmount("1"), 10000);
    }
}
