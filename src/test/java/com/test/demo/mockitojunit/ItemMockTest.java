package com.test.demo.mockitojunit;

import com.test.demo.mockitojunit.api.ItemController;
import com.test.demo.mockitojunit.entities.Item;
import com.test.demo.mockitojunit.services.ItemService;
import org.hamcrest.core.AllOf;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ItemController.class)
public class ItemMockTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ItemService service;

    @Test
    public void testGetAllItems() throws Exception {

        Item item = new Item();
        item.setId("6226279ad1669e86f4d1e2c7");
        item.setOrderId("");
        item.setName("Humble brush");
        item.setSku("P250");
        item.setQuantity(200);
        item.setPrice(150);
        List<Item> itemList = new ArrayList<>();
        itemList.add(item);

        when(service.getAll()).thenReturn(itemList);

        this.mockMvc.perform(
                get("/item"))
                .andDo(print())
                .andExpect(status().isOk())
                //.andExpect(content().json("[{'_id':'6226279ad1669e86f4d1e2c7','order_id':'','name':'Humble brush','sku':'P250','quantity':200,'price':150}]"));
                //.andExpect(jsonPath("$.[0].name").value("Humble brush"));
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name",
                AllOf.allOf(
                        StringContains.containsString("Humble brush")
                )));
    }

}
